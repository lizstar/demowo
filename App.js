import React from "react";
import { View, Button, Alert } from "react-native"
import MapView from "react-native-maps";

const initialLatLon = {
  latitude: 19.477705,
  longitude: -70.697254,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
}

const App = () => {
  const mapRef = React.createRef();

  return (
    <View style={{ flex: 1 }}>
      <MapView
        ref={mapRef}
        initialRegion={initialLatLon}
        style={{ flex: 1 }}
      />
      <View>
        <Button title="Go ahead..." onPress={() => mapRef.current.animateToRegion({
          latitude: 19.393163,
          longitude: -70.530923,
          latitudeDelta: 0.1,
          longitudeDelta: 0.1
        })} />
      </View>
    </View>
  );
};

export default App;